FROM openjdk:16-slim-buster

COPY requirements.txt ./

# Install Python
RUN apt-get update && apt-get install --yes python3 python3-pip

# Install dependencies
RUN pip3 install --no-cache-dir -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/opt/data_organizer_random_crop/src/"

COPY src /opt/data_organizer_random_crop/src/
ENTRYPOINT ["python3", "opt/data_organizer_random_crop/src/main.py"]

