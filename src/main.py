#!/usr/bin/env python3

import logging
import uuid
from typing import List

import pandas as pd
from learnfram.application.Application import Application
from learnfram.services.OrganizerComponent import OrganizerComponent
from sklearn.model_selection import train_test_split


class Service(OrganizerComponent):

    variables: List[str] = ["split_size"]
    component_type = "data_organizer_random_crop"

    def __init__(self, id: int, annotation_id: int, split_size: float):
        super(Service, self).__init__(id, annotation_id)
        self.split_size = split_size

    def run(self):

        logging.info(f"Strating ....")

        data_df = pd.DataFrame.from_dict(self.data["data"])
        annotation_df = pd.DataFrame.from_dict(self.data["annotation"])

        X_train, X_test, y_train, y_test = train_test_split(
            data_df, annotation_df, test_size=self.split_size, shuffle=False
        )

        data_df = pd.merge(data_df, X_train, how="left", indicator="is_train")
        data_df["is_train"] = (
            data_df["is_train"].map({"both": 0, "left_only": 1}).astype(int)
        )
        result = data_df["is_train"].to_list()

        file_name = "data_organizer_random_crop_{}.csv".format(uuid.uuid1())

        logging.info(f"The file {file_name} will be created")
        self.send(file_name, result)


if __name__ == "__main__":
    Application.start(Service)
